import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { RendezvousModel } from '../models/rendezvous-model';
import { Data } from '../models/data';

@Injectable({
  providedIn: 'root'
})
export class RendezvousService {

  private api = environment.api
  rendezvous: RendezvousModel[]
  rendezvous$= new Subject<RendezvousModel[]>()


  constructor(private http: HttpClient, private router: Router) { }

  getRendezVousMedecin(id: string){
    return new Promise((resolve, reject)=>{
      this.http.get<any>(this.api+'/rendezvous/rendezvousClin/'+id).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            this.rendezvous = data.result
            this.emitRendezVous()
          }else{
            console.log(data)
          }
        },
        (err)=>{
          console.log(err)
        }
      )
    })
  }

  emitRendezVous(){
    this.rendezvous$.next(this.rendezvous)
  }

  createRendezVous(date: string, utilisateur: string, animal: string ,clinique: string){
    return new Promise((resolve, reject)=>{
      this.http.post<any>(this.api+'/rendezvous/priseRdv'+utilisateur, {date: date, animal: animal, clinique: clinique}).subscribe(
        (data: Data)=>{
          if(data.status===201){
            this.getRendezVous(utilisateur)
            resolve(data)
          }
          else{
            reject(data.message)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  getRendezVous(utilisateur: string){
    return new Promise((resolve, reject)=>{
      this.http.get<any>(this.api+'/rendezvous/rdvUser/'+utilisateur).subscribe(
        (data: Data)=>{
          if(data.status===200){
            this.rendezvous = data.result
            this.emitRendezVous()
          }else{
            console.log(data)
          }
        },
        (err)=>{
          console.log(err)
        }
      )
    })
  }

  suppRendezVous(id: string, utilisateur: string){
    return new Promise((resolve, reject)=>{
      this.http.delete<any>(this.api+'/rendezvous/'+id).subscribe(
        ()=>{
          this.getRendezVous(utilisateur)
          resolve(true)
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  infoRdv(id: string){
    return new Promise((resolve, reject)=>{
      this.http.get<any>(this.api+'/rendezvous/'+id).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            resolve(data.result)
          }
          else{
            reject(data.message)
          }
        },
        (err)=>{
          reject(err)
        }

      )
    })
  }

  infoRdvClin(id: string){
    return new Promise((resolve, reject)=>{
      this.http.get<any>(this.api+'/rendezvous/rdvMed/'+id).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            resolve(data.result)
          }
          else{
            reject(data.message)
          }
        },
        (err)=>{
          reject(err)
        }

      )
    })
  }

  supprRdvClin(id: string, utilisateur: string){
    return new Promise((resolve, reject)=>{
      this.http.delete<any>(this.api+'/rendezvous/rdvMed/'+id).subscribe(
        ()=>{
          this.getRendezVousCli(utilisateur)
          resolve(true)
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  getRendezVousCli(utilisateur){
    return new Promise((resolve, reject)=>{
      this.http.get<any>(this.api+'/rendezvous/rdvClin/'+utilisateur).subscribe(
        (data: Data)=>{
          if(data.status===200){
            this.rendezvous = data.result
            this.emitRendezVous()
          }else{
            console.log(data)
          }
        },
        (err)=>{
          console.log(err)
        }
      )
    })
  }
}
