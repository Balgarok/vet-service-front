import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AnimalModel } from '../models/animal-model';
import { AnimalEntreesModel } from '../models/animal-entrees-model';
import { Data } from '../models/data';

@Injectable({
  providedIn: 'root'
})
export class AnimalService {

  private api = environment.api
  animaux: AnimalModel[]
  animaux$= new Subject<AnimalModel[]>()
  entreesAnimaux: AnimalEntreesModel[]
  entreesAnimaux$= new Subject<AnimalEntreesModel[]>()

  constructor(private http: HttpClient, private router: Router) { }

  getAnimaux(id: string){
    return new Promise((resolve, reject)=>{
      this.http.get<any>(this.api+'/animal/animaux/'+id).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            this.animaux = data.result
            this.emitAnimaux()
          }else{
            console.log(data)
          }
        },
        (err)=>{
          console.log(err)
        }
      )
    })
  }

  emitAnimaux(){
    this.animaux$.next(this.animaux)
  }

  createAnimal(nom: string, maitre: string, age: string, type: string, race: string){
    return new Promise((resolve, reject)=>{
      this.http.post<any>(this.api+'/animal/animal', {nom: nom, maitre: maitre, age: age, type: type, race : race}).subscribe(
        (data: Data)=>{
          if(data.status===201){
            this.getAnimaux(maitre)
            resolve(data)
          }
          else{
            reject(data.message)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  getAnimalById(id: string){
    return new Promise((resolve, reject)=>{
      this.http.get<any>(this.api+'/animal/animalById/'+id).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            resolve(data.result)
          }
          else{
            reject(data.message)
          }
        },
        (err)=>{
          reject(err)
        }

      )
    })
  }

  getEntreesCarnet(id: string){
    return new Promise((resolve, reject)=>{
      this.http.get<any>(this.api+'/animal/entrees/'+id).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            this.entreesAnimaux = data.result
            this.emitEntrees()
          }else{
            console.log(data)
          }
        },
        (err)=>{
          console.log(err)
        }
      )
    })
  }

  emitEntrees(){
    this.entreesAnimaux$.next(this.entreesAnimaux)
  }

  createEntreeCarnet(date: string, animal: string, entree: string){
    return new Promise((resolve, reject)=>{
      this.http.post<any>(this.api+'/animal/animalClin/'+animal, {date: date, animal: animal, entree: entree}).subscribe(
        (data: Data)=>{
          if(data.status===201){
            this.getEntreesCarnet(animal)
            resolve(data)
          }
          else{
            reject(data.message)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  modifierEntree(date: string, animal: string, entree: string){
    return new Promise((resolve, reject)=>{
      this.http.put<any>(this.api+'/animal/animalClin/'+animal, {date: date, animal: animal, entree: entree}).subscribe(
        (data: Data)=>{
          if(data.status===201){
            this.getEntreesCarnet(animal)
            resolve(data)
          }
          else{
            reject(data.message)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }
}
