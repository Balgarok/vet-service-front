import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login-clinique',
  templateUrl: './login-clinique.component.html',
  styleUrls: ['./login-clinique.component.css']
})
export class LoginCliniqueComponent implements OnInit {

  loginForm!: FormGroup
  errorMessage :string

  constructor(private formBuilder: FormBuilder, private router: Router, private auth: AuthService) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      nomClinique: [null, [Validators.required]],
      password: [null, [Validators.required]]
    })
  }

  onSubmit(){
    const nomCliniqueF = this.loginForm.get('nomClinique') as FormControl
    const passwordF = this.loginForm.get('password') as FormControl
    const nomClinique = nomCliniqueF.value
    const password = passwordF.value
    this.auth.login(nomClinique, password).then(
      ()=>{
        this.router.navigate(['/'])
      }
    ).catch(
      (err)=>{
        this.errorMessage = err.message
      }
    )
  }
}
