import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ReportingModel } from '../models/reporting-model';
import { Data } from '../models/data';

@Injectable({
  providedIn: 'root'
})
export class ReportingService {

  private api = environment.api
  reportings: ReportingModel[]
  reportings$= new Subject<ReportingModel[]>()

  constructor(private http: HttpClient, private router: Router) { }

  getReporting(){
    return new Promise((resolve, reject)=>{
      this.http.get<any>(this.api+'/bug/').subscribe(
        (data: Data)=>{
          if(data.status === 200){
            this.reportings = data.result
            this.emitReporting()
          }else{
            console.log(data)
          }
        },
        (err)=>{
          console.log(err)
        }
      )
    })
  }

  emitReporting(){
    this.reportings$.next(this.reportings)
  }

  bugById(id: string){
    return new Promise((resolve, reject)=>{
      this.http.get<any>(this.api+'/bug/'+id).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            resolve(data.result)
          }
          else{
            reject(data.message)
          }
        },
        (err)=>{
          reject(err)
        }

      )
    })
  }

  createReporting(date: string, page: string, description: string){
    return new Promise((resolve, reject)=>{
      this.http.post<any>(this.api+'/bug/reporting', {date: date, page: page, description: description}).subscribe(
        (data: Data)=>{
          if(data.status===201){
            resolve(data)
          }
          else{
            reject(data.message)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }
}
