import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimauxUtilisateurComponent } from './animaux-utilisateur.component';

describe('AnimauxUtilisateurComponent', () => {
  let component: AnimauxUtilisateurComponent;
  let fixture: ComponentFixture<AnimauxUtilisateurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnimauxUtilisateurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimauxUtilisateurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
