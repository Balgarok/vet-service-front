import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccueilAdministrationComponent } from './accueil-administration.component';

describe('AccueilAdministrationComponent', () => {
  let component: AccueilAdministrationComponent;
  let fixture: ComponentFixture<AccueilAdministrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccueilAdministrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccueilAdministrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
