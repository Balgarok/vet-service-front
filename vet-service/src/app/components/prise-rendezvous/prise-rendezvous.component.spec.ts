import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PriseRendezvousComponent } from './prise-rendezvous.component';

describe('PriseRendezvousComponent', () => {
  let component: PriseRendezvousComponent;
  let fixture: ComponentFixture<PriseRendezvousComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PriseRendezvousComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PriseRendezvousComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
