import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubsAdministrationComponent } from './subs-administration.component';

describe('SubsAdministrationComponent', () => {
  let component: SubsAdministrationComponent;
  let fixture: ComponentFixture<SubsAdministrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubsAdministrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubsAdministrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
