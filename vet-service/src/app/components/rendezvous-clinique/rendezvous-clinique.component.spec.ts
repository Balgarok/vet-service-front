import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RendezvousCliniqueComponent } from './rendezvous-clinique.component';

describe('RendezvousCliniqueComponent', () => {
  let component: RendezvousCliniqueComponent;
  let fixture: ComponentFixture<RendezvousCliniqueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RendezvousCliniqueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RendezvousCliniqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
