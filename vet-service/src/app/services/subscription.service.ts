import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Data } from '../models/data';
import { AbonnementModel } from '../models/abonnement-model';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {

  private api = environment.api
  subs: AbonnementModel[]
  subs$= new Subject<AbonnementModel[]>()

  constructor(private http: HttpClient, private router: Router) { }

  getSubscription(){
    return new Promise((resolve, reject)=>{
      this.http.get<any>(this.api+'/subs/subscription/').subscribe(
        (data: Data)=>{
          if(data.status === 200){
            this.subs = data.result
            this.emitSubs()
          }else{
            console.log(data)
          }
        },
        (err)=>{
          console.log(err)
        }
      )
    })
  }

  emitSubs(){
    this.subs$.next(this.subs)
  }

  priseSub(clinique: string, nomAbonnement: string, prixAbonnement: string){
    return new Promise((resolve, reject)=>{
      this.http.post<any>(this.api+'/subs/envoieSubClin', {clinique: clinique, nomAbonnement: nomAbonnement, prixAbonnement: prixAbonnement}).subscribe(
        (data: Data)=>{
          if(data.status===201){
            resolve(data)
          }
          else{
            reject(data.message)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  infoSubs(id: string){
    return new Promise((resolve, reject)=>{
      this.http.get<any>(this.api+'/subs/subscriptionClini/'+id).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            resolve(data.result)
          }
          else{
            reject(data.message)
          }
        },
        (err)=>{
          reject(err)
        }

      )
    })
  }

  infoSubsAdm(id: string){
    return new Promise((resolve, reject)=>{
      this.http.get<any>(this.api+'/subs/subsCliniqueAdmin/'+id).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            resolve(data.result)
          }
          else{
            reject(data.message)
          }
        },
        (err)=>{
          reject(err)
        }

      )
    })
  }
}
