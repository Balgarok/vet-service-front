import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimalUtilisateurComponent } from './animal-utilisateur.component';

describe('AnimalUtilisateurComponent', () => {
  let component: AnimalUtilisateurComponent;
  let fixture: ComponentFixture<AnimalUtilisateurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnimalUtilisateurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimalUtilisateurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
