import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginClientComponent } from './components/login-client/login-client.component';
import { LoginCliniqueComponent } from './components/login-clinique/login-clinique.component';
import { LoginAdministrationComponent } from './components/login-administration/login-administration.component';
import { AccueilUtilisateurComponent } from './components/accueil-utilisateur/accueil-utilisateur.component';
import { AccueilCliniqueComponent } from './components/accueil-clinique/accueil-clinique.component';
import { AccueilAdministrationComponent } from './components/accueil-administration/accueil-administration.component';
import { AnimauxUtilisateurComponent } from './components/animaux-utilisateur/animaux-utilisateur.component';
import { RendezvousUtilisateurComponent } from './components/rendezvous-utilisateur/rendezvous-utilisateur.component';
import { RendezvousCliniqueComponent } from './components/rendezvous-clinique/rendezvous-clinique.component';
import { CreationAnimalComponent } from './components/creation-animal/creation-animal.component';
import { AnimalUtilisateurComponent } from './components/animal-utilisateur/animal-utilisateur.component';
import { PriseRendezvousComponent } from './components/prise-rendezvous/prise-rendezvous.component';
import { ChoixAnimalRendezvousComponent } from './components/choix-animal-rendezvous/choix-animal-rendezvous.component';
import { AnimalCliniqueComponent } from './components/animal-clinique/animal-clinique.component';
import { InscriptionUtilisateurComponent } from './components/inscription-utilisateur/inscription-utilisateur.component';
import { InscriptionCliniqueComponent } from './components/inscription-clinique/inscription-clinique.component';
import { AnimauxCliniqueComponent } from './components/animaux-clinique/animaux-clinique.component';
import { SubcriptionCliniqueComponent } from './components/subcription-clinique/subcription-clinique.component';
import { SubsAdministrationComponent } from './components/subs-administration/subs-administration.component';
import { SubscriptionAdministrationComponent } from './components/subscription-administration/subscription-administration.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginClientComponent,
    LoginCliniqueComponent,
    LoginAdministrationComponent,
    AccueilUtilisateurComponent,
    AccueilCliniqueComponent,
    AccueilAdministrationComponent,
    AnimauxUtilisateurComponent,
    RendezvousUtilisateurComponent,
    RendezvousCliniqueComponent,
    CreationAnimalComponent,
    AnimalUtilisateurComponent,
    PriseRendezvousComponent,
    ChoixAnimalRendezvousComponent,
    AnimalCliniqueComponent,
    InscriptionUtilisateurComponent,
    InscriptionCliniqueComponent,
    AnimauxCliniqueComponent,
    SubcriptionCliniqueComponent,
    SubsAdministrationComponent,
    SubscriptionAdministrationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
