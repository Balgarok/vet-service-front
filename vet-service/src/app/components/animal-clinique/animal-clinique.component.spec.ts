import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimalCliniqueComponent } from './animal-clinique.component';

describe('AnimalCliniqueComponent', () => {
  let component: AnimalCliniqueComponent;
  let fixture: ComponentFixture<AnimalCliniqueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnimalCliniqueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimalCliniqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
