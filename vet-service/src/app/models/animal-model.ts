export class AnimalModel {
  _id: string
  nom: string
  maitre: string
  age: string
  type: string
  race: string
}
