import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimauxCliniqueComponent } from './animaux-clinique.component';

describe('AnimauxCliniqueComponent', () => {
  let component: AnimauxCliniqueComponent;
  let fixture: ComponentFixture<AnimauxCliniqueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnimauxCliniqueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimauxCliniqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
