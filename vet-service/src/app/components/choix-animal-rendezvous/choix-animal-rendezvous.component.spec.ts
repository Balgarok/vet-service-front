import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoixAnimalRendezvousComponent } from './choix-animal-rendezvous.component';

describe('ChoixAnimalRendezvousComponent', () => {
  let component: ChoixAnimalRendezvousComponent;
  let fixture: ComponentFixture<ChoixAnimalRendezvousComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChoixAnimalRendezvousComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoixAnimalRendezvousComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
