import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscriptionAdministrationComponent } from './subscription-administration.component';

describe('SubscriptionAdministrationComponent', () => {
  let component: SubscriptionAdministrationComponent;
  let fixture: ComponentFixture<SubscriptionAdministrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubscriptionAdministrationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscriptionAdministrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
