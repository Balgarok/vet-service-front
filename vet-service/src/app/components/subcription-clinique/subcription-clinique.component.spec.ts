import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubcriptionCliniqueComponent } from './subcription-clinique.component';

describe('SubcriptionCliniqueComponent', () => {
  let component: SubcriptionCliniqueComponent;
  let fixture: ComponentFixture<SubcriptionCliniqueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubcriptionCliniqueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubcriptionCliniqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
