import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginCliniqueComponent } from './login-clinique.component';

describe('LoginCliniqueComponent', () => {
  let component: LoginCliniqueComponent;
  let fixture: ComponentFixture<LoginCliniqueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginCliniqueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginCliniqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
