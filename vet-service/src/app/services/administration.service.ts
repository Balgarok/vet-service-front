import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { FactureModel } from '../models/facture-model';
import { Data } from '../models/data';
import { CliniqueModel } from '../models/clinique-model';

@Injectable({
  providedIn: 'root'
})
export class AdministrationService {

  private api = environment.api
  factures: FactureModel[]
  factures$= new Subject<FactureModel[]>()
  cliniques: CliniqueModel[]
  cliniques$= new Subject<CliniqueModel[]>()

  constructor(private http: HttpClient, private router: Router) {

  }

  getFactures(){
    this.http.get<any>(this.api + '/administration/factures').subscribe(
      (data: Data) => {
        if (data.status === 200) {
          this.factures = data.result
          this.emitFactures()
        }
        else {
          console.log(data)
        }
      },
      (err) => {
        console.log(err)
      }
    )
  }

  emitFactures() {
    this.factures$.next(this.factures)
  }

  getFactureById(id: string): Promise<FactureModel> {
    return new Promise((resole, reject) => {
      this.http.get<any>(this.api + '/administration/facture/' + id).subscribe(
        (data: Data) => {
          if (data.status === 200) {
            resole(data.result)
          }
          else {
            reject(data.message)
          }
        },
        (err) => {
          reject(err)
        }
      )
    })
  }

  getDemandes(){
    this.http.get<any>(this.api + '/administration/demandesInscription').subscribe(
      (data: Data) => {
        if (data.status === 200) {
          this.cliniques = data.result
          this.emitFactures()
        }
        else {
          console.log(data)
        }
      },
      (err) => {
        console.log(err)
      }
    )
  }

  emitCliniques(){
    this.cliniques$.next(this.cliniques)
  }

  getDemandesById(id: string): Promise<CliniqueModel>{
    return new Promise((resole, reject) => {
      this.http.get<any>(this.api + '/administration/demandeInsciption/' + id).subscribe(
        (data: Data) => {
          if (data.status === 200) {
            resole(data.result)
          }
          else {
            reject(data.message)
          }
        },
        (err) => {
          reject(err)
        }
      )
    })
  }

  validationDemande(id: string, clinique: CliniqueModel){
    return new Promise((resolve, reject)=>{
      this.http.put<any>(this.api+'/administration/validationInscription/'+id, {nomClinique: clinique.nomClinique, adresse: clinique.adresse, nomVet: clinique.nomVet, specialite: clinique.specialite, telephone: clinique.telephone}).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            resolve(data)
          }
          else{
            reject(data)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  refusValidation(id: string, clinique: CliniqueModel){
    return new Promise((resolve, reject)=>{
      this.http.put<any>(this.api+'/administration/refusInscription/'+id, {nomClinique: clinique.nomClinique, adresse: clinique.adresse, nomVet: clinique.nomVet, specialite: clinique.specialite, telephone: clinique.telephone}).subscribe(
        (data: Data)=>{
          if(data.status === 200){
            resolve(data)
          }
          else{
            reject(data)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }
}
