export class UtilisateurModel {
  _id: string
  username: string
  firstname: string
  lastname: string
  email: string
  password: string
  telephone: string
  address: string
}
