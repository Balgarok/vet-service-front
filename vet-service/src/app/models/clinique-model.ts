export class CliniqueModel {
  _id: string
  nomClinique: string
  adresse: string
  nomVet: string
  specialite: string
  telephone: string
  password: string
  horaires: string
  accepte: string
  dureeRdv: string
}
