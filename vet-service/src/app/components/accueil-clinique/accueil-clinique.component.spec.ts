import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccueilCliniqueComponent } from './accueil-clinique.component';

describe('AccueilCliniqueComponent', () => {
  let component: AccueilCliniqueComponent;
  let fixture: ComponentFixture<AccueilCliniqueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccueilCliniqueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccueilCliniqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
