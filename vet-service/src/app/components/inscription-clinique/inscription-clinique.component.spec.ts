import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InscriptionCliniqueComponent } from './inscription-clinique.component';

describe('InscriptionCliniqueComponent', () => {
  let component: InscriptionCliniqueComponent;
  let fixture: ComponentFixture<InscriptionCliniqueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InscriptionCliniqueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InscriptionCliniqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
