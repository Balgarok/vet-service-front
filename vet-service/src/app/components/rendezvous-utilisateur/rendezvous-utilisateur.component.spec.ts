import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RendezvousUtilisateurComponent } from './rendezvous-utilisateur.component';

describe('RendezvousUtilisateurComponent', () => {
  let component: RendezvousUtilisateurComponent;
  let fixture: ComponentFixture<RendezvousUtilisateurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RendezvousUtilisateurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RendezvousUtilisateurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
