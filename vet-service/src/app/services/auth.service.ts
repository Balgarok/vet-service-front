import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private api = environment.api
  token!: string
  userId!: string
  isAuth$ = new BehaviorSubject<boolean>(false)

  constructor(private http: HttpClient, private router: Router) {
    this.initAuth()
  }

  initAuth(){
    if(typeof localStorage !== "undefined"){
      const data = JSON.parse(localStorage.getItem('auth')as string)
      if(data!==null){
          this.userId = data.userId
          this.token = data.token
          this.isAuth$.next(true)

      }
    }
  }

  signup(email: string, password: string, firstname: string, lastname: string, username: string, address: string, telephone: string){
    return new Promise((resolve, reject)=>{
      this.http.post<any>(this.api+'/user/signup', {email: email, password: password, firstname: firstname, lastname: lastname, username : username, address :address, telephone : telephone}).subscribe(
        (signupData: {status: number, message: string})=>{
          if(signupData.status === 201){
            this.login(username, password)
            .then(()=>{
              resolve(true)
            })
            .catch((err)=>{
              reject(err)
            })
          }
          else{
            reject(signupData.message)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  signupCli(nomClinique : string, nomVet : string, specialite : string, email : string, adresse : string, telephone : string, horaires : string, dureeRdv : string, password: string){
    return new Promise((resolve, reject)=>{
      this.http.post<any>(this.api+'/user/signupCli', {email: email, password: password, nomClinique: nomClinique, nomVet: nomVet, specialite : specialite, adresse :adresse, telephone: telephone, horaires : horaires, dureeRdv : dureeRdv}).subscribe(
        (signupData: {status: number, message: string})=>{
          if(signupData.status === 201){
            this.loginCli(nomClinique, password)
            .then(()=>{
              resolve(true)
            })
            .catch((err)=>{
              reject(err)
            })
          }
          else{
            reject(signupData.message)
          }
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  loginCli(nomClinique: string, password: string){
    return new Promise((resolve, reject)=>{
      this.http.post<any>(this.api+'/user/loginClin', {nomClinique: nomClinique, password: password}).subscribe(
        (authData: {userId: string, token: string, type: string})=>{
          this.token = authData.token
          this.userId = authData.userId
          this.isAuth$.next(true)
          // save authData in local
          if(typeof localStorage !== 'undefined'){
            localStorage.setItem('auth', JSON.stringify(authData))
          }
          resolve(true)
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

  login(username: string, password: string){
    return new Promise((resolve, reject)=>{
      this.http.post<any>(this.api+'/user/login', {username: username, password: password}).subscribe(
        (authData: {userId: string, token: string, type: string})=>{
          this.token = authData.token
          this.userId = authData.userId
          this.isAuth$.next(true)
          // save authData in local
          if(typeof localStorage !== 'undefined'){
            localStorage.setItem('auth', JSON.stringify(authData))
          }
          resolve(true)
        },
        (err)=>{
          reject(err)
        }
      )
    })
  }

loginAdm(username: string, password: string){
  return new Promise((resolve, reject)=>{
    this.http.post<any>(this.api+'/user/loginAdm', {username: username, password: password}).subscribe(
      (authData: {userId: string, token: string, type: string})=>{
        this.token = authData.token
        this.userId = authData.userId
        this.isAuth$.next(true)
        // save authData in local
        if(typeof localStorage !== 'undefined'){
          localStorage.setItem('auth', JSON.stringify(authData))
        }
        resolve(true)
      },
      (err)=>{
        reject(err)
      }
    )
  })
}

  logout(){
    this.isAuth$.next(false)
    this.userId = null
    this.token = null
    if(typeof localStorage !== 'undefined'){
      localStorage.removeItem('auth')
    }
    return this.router.navigate(['/'])
  }

}
