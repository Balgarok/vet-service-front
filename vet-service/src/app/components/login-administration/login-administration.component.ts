import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login-administration',
  templateUrl: './login-administration.component.html',
  styleUrls: ['./login-administration.component.css']
})
export class LoginAdministrationComponent implements OnInit {

  loginForm!: FormGroup
  errorMessage :string

  constructor(private formBuilder: FormBuilder, private router: Router, private auth: AuthService) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      username: [null, [Validators.required]],
      password: [null, [Validators.required]]
    })
  }

  onSubmit(){
    const usernameF = this.loginForm.get('username') as FormControl
    const passwordF = this.loginForm.get('password') as FormControl
    const username = usernameF.value
    const password = passwordF.value
    this.auth.login(username, password).then(
      ()=>{
        this.router.navigate(['/'])
      }
    ).catch(
      (err)=>{
        this.errorMessage = err.message
      }
    )
  }

}
