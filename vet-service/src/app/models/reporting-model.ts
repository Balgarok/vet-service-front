export class ReportingModel {
  _id: string
  date: string
  page: string
  description: string
}
